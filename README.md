## Micrus Demo Project ##

This is an example project that demonstrates the usage and features of the [Micrus framework](http://micrus.avris.it).

Its documentation is available at **[docs.avris.it/micrus](http://docs.avris.it/micrus/)**.

### Instalation ###

Install [Composer](https://getcomposer.org/download/), move to the web directory of your server and run:

    composer create-project avris/micrus-demo

Composer will download the project with all of its dependencies.
It will ask you for server parameters (like database connection)
and will generate a secret encryption key unique for your app.

To generate database schema and load sample data, run:

    bin/micrus db:schema:create
	bin/micrus db:fixtures

And that's it. Your app is available under URL:

	http://localhost/micrus-demo/web/app_dev.php

### Running tests ###

	bin/micrus db:fixtures --force && phpunit

### Author ###

* ![Foto](http://avris.it/gfx/favicon.png)&nbsp;**Andrzej Prusinowski** [Avris.it](http://avris.it)
* ![Favicon](http://avris.it/dl/cc.png)&nbsp;Licence: [CC-BY](http://creativecommons.org/licenses/by/3.0/pl/)