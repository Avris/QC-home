<?php
namespace App\Form;

use App\Model\Execution;
use Avris\Micrus\Model\Form;
use Avris\Micrus\Model\Assert as Assert;

class ExecutionForm extends Form
{

    public function configure()
    {
        $this
            ->add('code', 'Textarea', [
                'label' => '',
                'attr' => ['placeholder' => 'Code'],
            ], [new Assert\NotBlank(), new Assert\MaxLength(4096)])
            ->add('input', 'Textarea', [
                'attr' => ['placeholder' => 'Input'],
            ], new Assert\MaxLength(4096))
            ->add('suite', 'Textarea', [
                'attr' => ['placeholder' => 'Suite'],
            ], new Assert\MaxLength(4096))
            ->add('options', 'ButtonChoice', [
                'choices' => Execution::$optionsNames,
                'multiple' => true,
            ])
        ;
    }
}