<?php
namespace App\Controller;

use App\Form\ExecutionForm;
use App\Model\Execution;
use Avris\Micrus\Controller\Controller;
use Avris\Micrus\View\FormStyle\Bootstrap;
use Avris\QC\QC;
use Avris\QC\ReferenceGenerator;

class HomeController extends Controller
{
    public function homeAction()
    {
        return $this->render();
    }

    public function runAction(Execution $execution = null)
    {
        if (!$execution || $this->getRequest()->getData('save')) {
            $execution = new Execution();
        }

        $form = new ExecutionForm($execution);
        $form->bindRequest($this->getRequest());
        if ($form->isValid()) {
            set_time_limit(15);
            $execution->execute(new QC());
        }

        if ($this->getRequest()->getData('save')) {
            $this->getEm()->persist($execution);
            $this->getEm()->flush();
        }

        return $this->render([
            '_view' => $this->getRequest()->isAjax() ? 'Home/output.html.twig' : null,
            'form' => $form->setStyle(new Bootstrap()),
            'execution' => $execution,
            'reference' => $this->getReference(),
            'demos' => $this->getEm()->getRepository('Execution')->findDemos(),
        ]);
    }

    protected function getReference()
    {
        return $this->getService('cacher')->cache('reference', function() {
            return (new ReferenceGenerator())->generateHtml(
                null,
                'reference table table-condensed',
                'insert btn btn-default btn-xs'
            );
        });
    }

}