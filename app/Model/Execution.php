<?php
namespace App\Model;

use Avris\QC\AsideOutput\AccumulativeOutput;
use Avris\QC\Exception\ErrorHandler;
use Avris\QC\Exception\QCException;
use Avris\QC\Helper;
use Avris\QC\QC;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Model\ExecutionRepository")
 * @ORM\Table(name="execution")
 */
class Execution
{
    public static $optionsNames = [
        'debug' => '<span class="fa fa-bug"></span> Show debug info',
        'suite' => '<span class="fa fa-list"></span> Test suite mode',
    ];

    /**
     * @var string
     * @ORM\Column(type="string")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Service\RandomIdGenerator")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=4096)
     */
    protected $code;

    /**
     * @var string
     * @ORM\Column(type="string", length=4096)
     */
    protected $input;

    /**
     * @var string
     * @ORM\Column(type="string", length=4096)
     */
    protected $suite;

    /**
     * @var array
     * @ORM\Column(type="json_array")
     */
    protected $options = [];

    /**
     * @var string
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $demoName;

    /**
     * @var bool
     */
    protected $status;

    /**
     * @var mixed
     */
    protected $result;

    /**
     * @var string
     */
    protected $output;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     * @return Execution
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getInput()
    {
        return $this->input;
    }

    /**
     * @param string $input
     * @return Execution
     */
    public function setInput($input)
    {
        $this->input = $input;
        return $this;
    }

    /**
     * @return string
     */
    public function getSuite()
    {
        return $this->suite;
    }

    /**
     * @param string $suite
     * @return Execution
     */
    public function setSuite($suite)
    {
        $this->suite = $suite;
        return $this;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param array $options
     * @return $this
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isSuiteMode()
    {
        return in_array('suite', $this->options);
    }

    /**
     * @return boolean
     */
    public function isDebugMode()
    {
        return in_array('debug', $this->options);
    }

    /**
     * @return string
     */
    public function getDemoName()
    {
        return $this->demoName;
    }

    /**
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return string
     */
    public function getOutput()
    {
        return $this->output;
    }

    public function getCodeSize()
    {
        $handler = new ErrorHandler();
        $size = Helper::codeSize($this->code);
        $handler->destruct();
        return $size;
    }

    /**
     * @param QC $qc
     * @return $this
     */
    public function execute(QC $qc)
    {
        try {
            $this->output = new AccumulativeOutput($this->isDebugMode() ? 100 : -1);
            if ($this->isSuiteMode()) {
                $this->result = $qc->runTestsuite(
                    $this->code,
                    Helper::buildSuiteFromCode($this->suite),
                    $this->output
                );
                $this->status = $this->result['ok'];
            } else {
                $output = $this->result = $qc->run(
                    $this->code,
                    Helper::parseInput($this->input),
                    $this->output
                );
                $this->result = Helper::dumpValue($output, true);
                $this->status = true;
            }
        } catch (QCException $e) {
            $this->result = $e->getMessage();
            $this->status = false;
        }

        return $this;
    }
}
