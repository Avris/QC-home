<?php
namespace App\Model;

use Doctrine\ORM\EntityRepository;

class ExecutionRepository extends EntityRepository
{
    public function findDemos()
    {
        return $this->createQueryBuilder('e')
            ->where('e.demoName IS NOT NULL')
            ->getQuery()->getResult();
    }
}
