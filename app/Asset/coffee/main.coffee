articles = $('article')
body = $('html, body')

resize = ->
  articles.css('height', '')
  articles.each (i) ->
    $(this).css('height', Math.max($(this).height(), window.innerHeight))
resize()
$(window).resize(resize)

if $('[data-slide]').length
  $('a').click ->
    href = $(this).attr('href')
    console.log(href)
    if href.substr(0,1) == '#'
      top = $(href).position().top
      body.animate({scrollTop: top})
      return false

  getMinByDiff = (slides) ->
    min = { top: 99999, diff: 9999 }
    for slide in slides
      if Math.abs(slide.diff) < Math.abs(min.diff)
        min = slide
    return min

  findNearest = ->
    sT = $(document).scrollTop()
    slides = []
    $('[data-slide]').each (i) ->
      top = $(this).position().top
      slides.push { top: top, diff: sT - top, last: $(this).data('slide') == 'last' }
    return getMinByDiff(slides)

  scrollToNearest = ->
    return if window.innerWidth < 760
    min = findNearest()
    return if min.last and min.diff > 0
    clearTimeout(scrollTimeout)
    scrollTimeout = true
    body.animate({scrollTop: min.top}, 400, -> scrollTimeout = null)

  scrollTimeout = null
  $(document).scroll ->
    clearTimeout(scrollTimeout) if scrollTimeout
    scrollTimeout = setTimeout(->
      scrollToNearest()
      scrollTimeout = null
    , 1000)

$.fn.selectRange = (start, end = start) ->
  @each ->
    if 'selectionStart' of this
      @selectionStart = start
      @selectionEnd = end
    else if @setSelectionRange
      @setSelectionRange start, end
    else if @createTextRange
      range = @createTextRange()
      range.collapse true
      range.moveEnd 'character', end
      range.moveStart 'character', start
      range.select()
    return

code = $('#execution_code').focus()
$('textarea').focus -> code = $(@)

$('form table button').click ->
  insert = $(this).data('insert')

  caretPos = code[0].selectionStart;
  text = code.val();
  code.val text.substring(0, caretPos) + insert + text.substring(caretPos)
  code.selectRange caretPos + 1
  code.focus()

$.expr[':'].contains = (a, i, m) ->
  $(a).text().toLowerCase().indexOf(m[3].toLowerCase()) >= 0

$.expr[':'].attrContains = (a, i, m) ->
  args = m[3].split(',').map (arg) ->
    arg.replace /^\s*["']|["']\s*$/g, ''
  $(a).attr(args[0]).toLowerCase().indexOf(args[1].toLowerCase()) >= 0

searchInput = $('#search')
lines = $('tr:has(td)')
tables = $('table')
search = ->
  text = searchInput.val().replace('"', '\\"')
  tables.removeClass 'hidden'
  if not text
    lines.removeClass 'hidden'
    return true
  lines.addClass 'hidden'
  lines.filter(':contains("'+text+'")').removeClass 'hidden'
  tables.filter(':attrContains(data-section, "'+text+'")').find('tr').removeClass 'hidden'
  tables.each (i, el) ->
    if $(el).find('tr:not(.hidden)').length == 1
      $(el).addClass 'hidden'
  return true

searchInput.keyup search
searchInput.next().click search

$.fn.isValid = ->
  this[0].checkValidity()

$.fn.formData = (btn) ->
  data = $(this).serializeArray().reduce(((obj, item) -> obj[item.name] = item.value; obj), {})
  if btn then data[btn.attr('name')] = btn.attr('name')
  data

form = $('form')
output = $('#output')
form.submit (e) ->
  if not $(this).isValid() then return false
  output.html '<p class="text-center"><span class="fa fa-spinner fa-pulse fa-4x"></span></p>'
  $.ajax
    type: "POST"
    url: form.attr('action')
    data: form.formData($(this).find("button[type=submit]:focus"))
    success: (data) ->
      output.html data
      url = output.find('[data-url]').data('url')
      if window.history.pushState
        window.history.pushState(null, null, url)
    error: ->
      output.html '<div class="alert alert-danger"><strong>Execution failed!</strong><pre>Please note, that the maximum execution time is 15 seconds...</pre></div>'
  false

$('a[data-confirm]').click ->
  confirm $(this).data('confirm')

$isSuite = $('input[name=Execution\\\[options\\\]\\\[suite\\\]]')
$textInput = $('textarea[name=Execution\\\[input\\\]]').parent().parent().parent()
$textSuite = $('textarea[name=Execution\\\[suite\\\]]').parent().parent().parent()

updateInputSuite = ->
  if $isSuite.is(':checked')
    $textInput.addClass 'hidden'
    $textSuite.removeClass 'hidden'
  else
    $textInput.removeClass 'hidden'
    $textSuite.addClass 'hidden'
updateInputSuite()
$isSuite.change updateInputSuite
