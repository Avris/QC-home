<?php
if (isset($_SERVER['HTTP_CLIENT_IP'])
    || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
    || php_sapi_name() == 'cli-server'
    || !(in_array(@$_SERVER['REMOTE_ADDR'], ['127.0.0.1', 'fe80::1', '::1']))
) {
    header('HTTP/1.0 403 Forbidden');
    exit('This file can only be accessed on localhost');
}

require_once __DIR__ . '/../vendor/autoload.php';
$app = new Avris\Micrus\App('dev');
$response = $app->run();
$response->send();
